package nl.thomaskuijs.banix.Commands;

import nl.thomaskuijs.banix.BanixPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Deze class is gemaakt door Thomas Kuijs op 11/06/2017.
 */
public class Commands implements CommandExecutor {

    public static void registerCommands() {
        BanixPlugin.getPlugin().getCommand("banix").setExecutor(new Commands());
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(s.equalsIgnoreCase("banix")) {
            Banix.executeCommand(commandSender);
        }
        return false;
    }
}
