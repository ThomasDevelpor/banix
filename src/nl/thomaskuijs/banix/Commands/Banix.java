package nl.thomaskuijs.banix.Commands;

import nl.thomaskuijs.banix.BanixPlugin;
import nl.thomaskuijs.banix.Chat;
import org.bukkit.command.CommandSender;

import static nl.thomaskuijs.banix.Chat.HIGHLIGHT_COLOR;
import static nl.thomaskuijs.banix.Chat.MESSAGE_COLOR;

/**
 * Deze class is gemaakt door Thomas Kuijs op 11/06/2017.
 */
public class Banix {

    public static void executeCommand(CommandSender sender) {
        if(sender.hasPermission("banix.help")) {
            sender.sendMessage(Chat.acc("" +
                    "&6----==[&eBanix&6]==----\n" +
                    "&6/ban&e [-s] <player> [reason]\n" +
                    "&6/tempban&e [-s] <time> <player> [reason]\n" +
                    "&6/mute&e [-s] <player> [reason]\n" +
                    "&6/tempmute&e [-s] <time> <player> [reason]\n" +
                    "&6/warn&e [-s] <player> [reason]\n" +
                    "&6/kick&e [-s] <player> [reason]\n" +
                    "&6/clearchat&e [-s] [reason]\n" +
                    "&6/chatmodus&e [-s] <staff|vip|all>\n" +
                    "&6<>&e: required &6[]&e: optional"));
        } else {
            sender.sendMessage(Chat.formatMessage(HIGHLIGHT_COLOR + "Banix" + MESSAGE_COLOR + " plugin version " +
                    HIGHLIGHT_COLOR + BanixPlugin.getPlugin().getDescription().getVersion() + MESSAGE_COLOR + " created by " +
                    HIGHLIGHT_COLOR + "Thomasix" + MESSAGE_COLOR + "!"));
        }
    }
}
