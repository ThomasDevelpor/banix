package nl.thomaskuijs.banix;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

import static nl.thomaskuijs.banix.Chat.ERROR_COLOR;
import static nl.thomaskuijs.banix.Chat.ERROR_HIGHLIGHT_COLOR;
import static nl.thomaskuijs.banix.Chat.MESSAGE_COLOR;
import static nl.thomaskuijs.banix.Chat.PLUGIN_COLOR;

/**
 * Deze class is gemaakt door Thomas Kuijs op 11/06/2017.
 */
public class MessageFile {

    private static File file = new File(BanixPlugin.getPlugin().getDataFolder(), File.separator + "messages.yml");
    private static FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

    public static String getMessage(String message) {
        return fileConfig.getString("messages." + message);
    }
    public static void createYaml() {
        if(!file.exists()) {
            fileConfig.set("config-version", 1);
            fileConfig.set("messages.prefix", PLUGIN_COLOR + "[Banix] " + MESSAGE_COLOR);
            fileConfig.set("messages.muted.talk.perm", ERROR_COLOR + "You are still muted for "
                    + ERROR_HIGHLIGHT_COLOR + "%reason%" + ERROR_COLOR + "!");
            fileConfig.set("messages.muted.talk.temp", ERROR_COLOR + "You are still muted for "
                    + ERROR_HIGHLIGHT_COLOR + "%reason%" + ERROR_COLOR + "!");
            fileConfig.set("messages.reload.error", ERROR_COLOR + "Reload is disabled by Banix!");
            saveYaml();
        }
    }
    private static void saveYaml() {
        try {
            fileConfig.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
