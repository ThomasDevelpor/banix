package nl.thomaskuijs.banix.FileType.Ban;

import nl.thomaskuijs.banix.BanixPlugin;
import nl.thomaskuijs.banix.ConfigFile;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;

/**
 * Deze class is gemaakt door Thomas Kuijs op 11/06/2017.
 */
public class Ban {

    private String reason;
    private long unbanTime;
    private String bannedBy;
    private boolean banned;
    private Player player;

    public void Ban(Player p) {
        player = p;
        if(ConfigFile.useMysql) {

        } else {
            File file = new File(BanixPlugin.getPlugin().getDataFolder(), File.separator + "bans.yml");
            FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

            if(fileConfig.contains(p.getUniqueId().toString())) {
                banned = true;
                if(fileConfig.contains(p.getUniqueId().toString() + ".bannedby")) {
                    bannedBy = fileConfig.getString(p.getUniqueId().toString() + ".bannedby");
                }
                if(fileConfig.contains(p.getUniqueId().toString() + ".unbantime")) {
                    unbanTime = fileConfig.getLong(p.getUniqueId().toString() + ".unbantime");
                }
                if(fileConfig.contains(p.getUniqueId().toString() + ".reason")) {
                    reason = fileConfig.getString(p.getUniqueId().toString() + ".reason");
                }
                if(unbanTime != 0) {
                    if(unbanTime <= System.currentTimeMillis()) {
                        //unban
                        fileConfig.set(p.getUniqueId().toString() + ".reason", null);
                        fileConfig.set(p.getUniqueId().toString() + ".unbantime", null);
                        fileConfig.set(p.getUniqueId().toString() + ".bannedby", null);
                        fileConfig.set(p.getUniqueId().toString(), null);
                        banned = false;
                    }
                }
            }
        }
    }

    public void unban() {
        File file = new File(BanixPlugin.getPlugin().getDataFolder(), File.separator + "bans.yml");
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
        fileConfig.set(player.getUniqueId().toString() + ".reason", null);
        fileConfig.set(player.getUniqueId().toString() + ".unbantime", null);
        fileConfig.set(player.getUniqueId().toString() + ".bannedby", null);
        fileConfig.set(player.getUniqueId().toString(), null);
        banned = false;
    }

    public String getReason() {
        return reason;
    }

    public String getBannedBy() {
        return bannedBy;
    }

    public long getUnbanTime() {
        return unbanTime;
    }

    public boolean isBanned() {
        return banned;
    }

    public Player getPlayer() {
        return player;
    }

    public void ban(String reason, String bannedBy) {
        File file = new File(BanixPlugin.getPlugin().getDataFolder(), File.separator + "bans.yml");
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
        unbanTime = 0;
        this.bannedBy = bannedBy;
        this.reason = reason;
        banned = true;

        fileConfig.set(player.getUniqueId().toString() + ".reason", null);
        fileConfig.set(player.getUniqueId().toString() + ".unbantime", null);
        fileConfig.set(player.getUniqueId().toString() + ".bannedby", null);
        fileConfig.set(player.getUniqueId().toString(), null);
    }
    public void ban(String reason, String bannedBy, Long banLengthInMillis) {
        File file = new File(BanixPlugin.getPlugin().getDataFolder(), File.separator + "bans.yml");
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
        unbanTime = System.currentTimeMillis() + banLengthInMillis;
        this.bannedBy = bannedBy;
        this.reason = reason;
        banned = true;

        fileConfig.set(player.getUniqueId().toString() + ".reason", reason);
        fileConfig.set(player.getUniqueId().toString() + ".unbantime", unbanTime);
        fileConfig.set(player.getUniqueId().toString() + ".bannedby", bannedBy);
    }
}
