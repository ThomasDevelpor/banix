package nl.thomaskuijs.banix.Events;

import nl.thomaskuijs.banix.BanixPlugin;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

/**
 * Deze class is gemaakt door Thomas Kuijs op 11/06/2017.
 */
public class Events {

    public static void registerEvents() {

    }

    public static void addEvent(Listener listener) {
        Bukkit.getPluginManager().registerEvents(listener, BanixPlugin.getPlugin());
    }
}
