package nl.thomaskuijs.banix;

import org.bukkit.Bukkit;

/**
 * Deze class is gemaakt door Thomas Kuijs op 10/06/2017.
 */
public class Chat {

    private static char COLOR_CHAR = '\u0026';

    public static String HIGHLIGHT_COLOR = COLOR_CHAR + "e";
    public static String MESSAGE_COLOR = COLOR_CHAR + "f";
    public static String PLUGIN_COLOR = COLOR_CHAR + "6";
    public static String ERROR_COLOR = COLOR_CHAR + "c";
    public static String ERROR_HIGHLIGHT_COLOR = COLOR_CHAR + "4";

    private static String prefix = PLUGIN_COLOR + "[Banix] " + MESSAGE_COLOR;
    private static String consolePrefixInfo = "[BanixInfo] ";
    private static String consolePrefixError = "[BanixError] ";


    public static String formatMessage(String message) {
        return acc(prefix + message);
    }
    public static String alternativeColorCodes(String message) {
        return message.replace('\u0026', '\u00A7');
    }
    public static String acc(String message) {
        return alternativeColorCodes(message);
    }

    public static void sendConsoleInfo(String message) {
        Bukkit.getConsoleSender().sendMessage(consolePrefixInfo + message);
    }
    public static void sendConsoleError(String message) {
        Bukkit.getConsoleSender().sendMessage(consolePrefixError + message);
    }

    public static void updatePrefix() {
        prefix = MessageFile.getMessage("prefix");
    }
}
