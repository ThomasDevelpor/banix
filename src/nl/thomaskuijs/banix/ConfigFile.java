package nl.thomaskuijs.banix;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Deze class is gemaakt door Thomas Kuijs op 11/06/2017.
 */
public class ConfigFile {

    private static File file = new File(BanixPlugin.getPlugin().getDataFolder(), File.separator + "config.yml");
    private static FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

    public static boolean useMysql;

    public static void loadConfig() {
        useMysql = fileConfig.getBoolean("mysql.use");
    }

    public static FileConfiguration getFileConfiguration() {return fileConfig;}
    public static void createYaml() {
        if(!file.exists()) {
            fileConfig.set("config-version", 1);
            fileConfig.set("mysql.use", false);
            fileConfig.set("mysql.username", "username");
            fileConfig.set("mysql.database", "database");
            fileConfig.set("mysql.host", "host");
            fileConfig.set("mysql.password", "password");
            saveYaml();
        }
    }
    public static void saveYaml() {
        try {
            fileConfig.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
