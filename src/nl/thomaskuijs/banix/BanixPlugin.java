package nl.thomaskuijs.banix;

import nl.thomaskuijs.banix.Commands.Commands;
import nl.thomaskuijs.banix.Events.Events;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Deze class is gemaakt door Thomas Kuijs op 10/06/2017.
 */
public class BanixPlugin extends JavaPlugin {

    private static BanixPlugin banixPlugin;
    public static BanixPlugin getPlugin() {return banixPlugin;}

    public void onEnable() {
        Chat.sendConsoleInfo("Plugin is starting up...");

        //defining plugin instance
        banixPlugin = this;

        //registering events
        Events.registerEvents();

        //regsiter commands
        Commands.registerCommands();

        //create messagesYaml
        MessageFile.createYaml();

        //create configYaml
        ConfigFile.createYaml();

        //load configYaml
        ConfigFile.loadConfig();

        //getting prefix
        Chat.updatePrefix();

        Chat.sendConsoleInfo("Plugin is started up...");
    }

    public void onDisable() {
        Chat.sendConsoleInfo("Plugin is shuting down...");
    }
}
